import json
import pandas as pd
import time
import boto3
from sqlalchemy import create_engine
import psycopg2

def lambda_handler(event, context):
    s3 = boto3.client("s3",region_name="us-east-1",aws_access_key_id="",aws_secret_access_key="")
    srcFileName="APIs/ApiTwitter/Json/03-12-2020/tweets.json"
    destFileName="/tmp/tw.json"
    bucketName="bucketparteb"
    s3.download_file(bucketName,srcFileName,destFileName)
    
    with open('/tmp/tw.json','r') as f:
        data =json.loads(f.read())

    df=pd.DataFrame(data)

    tweets=pd.DataFrame(columns=['FechaTweet','Texto','Usuario_Id','RetweetsCount','FavoritesCount','Idioma','Origen','FechaConsulta','FechaCargue','Evento'])
    users=pd.DataFrame(columns=['Usuario_Id','NombreUsuario','Localizacion','Followers','Followes','Origen','FechaConsulta','FechaCargue','Evento'])

    for i in df.index:
        tweets.loc[i]=[df['created_at'][i],df['full_text'][i],df['user'][i]['id'],df['retweet_count'][i],df['favorite_count'][i],df['lang'][i],"Tweeter/Tweet","",time.time(),str(event)]
        users.loc[i]=[df.loc[i]['user']['id'],df.loc[i]['user']['screen_name'],df.loc[i]['user']['location'],df.loc[i]['user']['followers_count'],df.loc[i]['user']['friends_count'],"Tweeter/Tweet/User","",time.time(),str(event)]

    #print(tweets)
    #print(users)
    engine = create_engine('postgresql://postgres:Azzurri1-@tweeterbd.ct1t2hnyglxk.us-east-1.rds.amazonaws.com/postgres')
    tweets.to_sql('tweets', con=engine, index=False, if_exists='replace')
    print("TweetOK")
    users.to_sql('users', con=engine, index=False, if_exists='replace')
    print("UserOk")
    
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }