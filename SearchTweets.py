import json
import keys
import tweepy
import time

#autenticacion de credenciales
auth = tweepy.OAuthHandler(keys.consumer_key,
                           keys.consumer_secret)
auth.set_access_token(keys.access_token,
                      keys.access_token_secret)
api = tweepy.API(auth, wait_on_rate_limit=False,
                 wait_on_rate_limit_notify=False,)

# endpoint 1, consulta y obtiene tweets de acuerdo a una palabra clave  ej: 'covid'
with open('tweets.json', 'w') as f:
        f.write('[')
for tweet in tweepy.Cursor(api.search,q="Covid",tweet_mode='extended').items(2000):
    with open('tweets.json', 'a') as f:
        f.write(json.dumps(tweet._json, indent=2))
        f.write(',')
with open('tweets.json', 'a') as f:
        f.write(']')

print("Tweets Ok")

#endpoint 2, obtener tendencias por ubicacion, usa woeid como parametro de localizacion
trend=api.trends_place(id=23424787)
with open('trends.json','a',encoding='utf8') as f:
    f.write(json.dumps(trend, indent=2))
print("Trends Ok")


#endpoint 3, obtener los de seguidores de una cuenta particular a partir de su nombre de usuario
with open('users.json', 'w') as f:
        f.write('[')
for user in tweepy.Cursor(api.followers, screen_name="WHO").items(200):
   with open('users.json', 'a') as f:
        f.write(json.dumps(user._json, indent=2))
        f.write(',')
with open('users.json', 'a') as f:
        f.write(']')
print("Users Ok")       
