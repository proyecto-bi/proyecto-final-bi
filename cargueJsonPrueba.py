import json
import pandas as pd
import time

with open('tweets.json','r') as f:
    data =json.loads(f.read())

df=pd.DataFrame(data)

tweets=pd.DataFrame(columns=['FechaTweet','Texto','Usuario_Id','RetweetsCount','FavoritesCount','Idioma','Origen','FechaConsulta','FechaCargue','Evento'])
users=pd.DataFrame(columns=['Usuario_Id','NombreUsuario','Localizacion','Followers','Followes','Origen','FechaConsulta','FechaCargue','Evento'])

for i in df.index:
    tweets.loc[i]=[df['created_at'][i],df['full_text'][i],df['user'][i]['id'],df['retweet_count'][i],df['favorite_count'][i],df['lang'][i],"Tweeter/Tweet","",time.time(),""]
    users.loc[i]=[df.loc[i]['user']['id'],df.loc[i]['user']['screen_name'],df.loc[i]['user']['location'],df.loc[i]['user']['followers_count'],df.loc[i]['user']['friends_count'],"Tweeter/Tweet/User","",time.time(),""]

print(tweets)
print(users)