# Proyecto Final BI
## Universidad del Quindio

Implementación de una consulta a la API de Twitter

## Herramientas

Como lenguaje de programacion usamos Python.

```bash
sudo apt-get install python
```

Para la interaccion con la Api, usaremos la libreria Tweepy, podemos usar [pip](https://pip.pypa.io/en/stable/) para instarla.

```bash
pip install tweepy
```
Ademas de GitLab como repositorio y Visual Studio Code como entorno de desarrollo.
## Endpoints

A traves de la herramienta Tweepy podemos consultar:

- Búsquedas de contenido
- Actividad de una cuenta
- Mensajes directos
- Contenido realtime
- Embeber tweets
- Anuncios

Para este proyecto se realiza busqueda de contenido, especificamente Tweets relacionados con una tematica de interes, seguidores de una cuenta y tendencias por localizacion


## Colaboradores

- [Jorge Mario Burbano](https://gitlab.com/jmburbano)
- [Cristian David Toro](https://gitlab.com/crismachianno)